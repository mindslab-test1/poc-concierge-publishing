import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:shinhan/components/main_button_bottom.dart';
import 'package:shinhan/components/side_menu_popup.dart';

class MainButtonTop extends StatefulWidget {
  @override
  _MainButtonsState createState() => _MainButtonsState();
}

class _MainButtonsState extends State<MainButtonTop> {

  TextSpan blueTitleSpan1(String text){
    return TextSpan(
        text:text,
        style: TextStyle(
          fontFamily: 'nanumSquare',
          fontSize: MediaQuery.of(context).size.width*0.020327,
          fontWeight: FontWeight.w700,
          height:1.09,
          letterSpacing: -(MediaQuery.of(context).size.width*0.0009167),
          color: Color(0xff084da3),
        )
    );
  }

  TextSpan blueTitleSpan2(String text){
    return TextSpan(
        text:text,
        style: TextStyle(
          fontFamily: 'nanumSquare',
          fontSize: MediaQuery.of(context).size.width*0.020327,
          fontWeight: FontWeight.w400,
          height:1.09,
          letterSpacing: -(MediaQuery.of(context).size.width*0.0009167),
          color: Color(0xff084da3),
        )
    );
  }

  TextSpan whiteTitleSpan1(String text){
    return TextSpan(
        text:text,
        style: TextStyle(
          fontFamily: 'nanumSquare',
          fontSize: MediaQuery.of(context).size.width*0.020327,
          fontWeight: FontWeight.w700,
          height:1.09,
          letterSpacing: -(MediaQuery.of(context).size.width*0.0009167),
          color: Colors.white,
        )
    );
  }

  TextSpan whiteTitleSpan2(String text){
    return TextSpan(
        text:text,
        style: TextStyle(
          fontFamily: 'nanumSquare',
          fontSize: MediaQuery.of(context).size.width*0.020327,
          fontWeight: FontWeight.w400,
          height:1.09,
          letterSpacing: -(MediaQuery.of(context).size.width*0.0009167),
          color: Colors.white,
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;

    double height = mediaQuery.size.height;

    return Container(
      padding: EdgeInsets.only(right:width*0.037),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SizedBox(
                  height: height*0.29427083,
                ),
                Stack(
                  children: [
                    Container(
                      width: width*0.4037,
                      child: Column(
                        children: [
                          ClipRect(
                            child: BackdropFilter(
                              filter: ImageFilter.blur(
                                sigmaX: width*0.00565,
                                sigmaY: width*0.00556,
                              ),
                              child: Container(
                                padding: EdgeInsets.fromLTRB(width*0.018056, height*0.0104167, width*0.018056, 0),
                                height: height*0.14583,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: width*0.000462963,
                                    color: Color.fromRGBO(255, 255, 255, 0.25),
                                  ),
                                  color: Colors.white.withOpacity(0.35),
                                  // borderRadius: BorderRadius.circular(width*0.01852),
                                ),
                                child: Column(
                                  crossAxisAlignment:CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      padding: EdgeInsets.only(left: width*0.0111),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          RichText(text: TextSpan(
                                              children: [
                                                blueTitleSpan1('입출금'),
                                                blueTitleSpan2('창구')
                                              ]
                                          ),),
                                        ],
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        BlueButton(
                                            text : ["입금","/","출금","/","송금"],
                                            buttonNum:  [1,2,1,2,1]
                                        ),
                                        BlueButton(
                                            text : ["신용카드","/","체크카드\n","신규 및 재발급"],
                                            buttonNum:  [1,2,1,1]
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        BlueButton(
                                            text : ["통장","/","인감분실","/\n","재발행"],
                                            buttonNum:   [1,2,1,2,1]
                                        ),
                                        BlueButton(
                                            text : ["인터넷","/\n","스마트폰 뱅킹"],
                                            buttonNum:  [1,2,1]
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: height*0.01667),
                          ClipRect(
                            child: BackdropFilter(
                              filter: ImageFilter.blur(
                                sigmaX: width*0.00565,
                                sigmaY: width*0.00565,
                              ),
                              child: Container(
                                padding: EdgeInsets.fromLTRB(width*0.018056, height*0.0104167, width*0.018056, 0),
                                height: height*0.089583,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: width*0.000462963,
                                    color: Color.fromRGBO(255, 255, 255, 0.25),
                                  ),
                                  color: Colors.white.withOpacity(0.35),
                                  // borderRadius: BorderRadius.circular(width*0.01852),
                                ),
                                child: Column(
                                  crossAxisAlignment:CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      padding: EdgeInsets.only(left:width*0.0111),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          RichText(text: TextSpan(
                                              children: [
                                                whiteTitleSpan1("화상상담"),
                                                whiteTitleSpan2("창구"),
                                              ]
                                          ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        BlueButton(
                                            text : ["정기예금","/","적금","/","청약\n신규","해지"],
                                            buttonNum: [4,5,4,5,4,4]
                                        ),
                                        BlueButton(
                                            text : ["개인신용대출"],
                                            buttonNum: [1]
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: height*0.01667),
                          ClipRect(
                            child: BackdropFilter(
                              filter: ImageFilter.blur(
                                sigmaX: width*0.00565,
                                sigmaY: width*0.00565,
                              ),
                              child: Container(
                                padding: EdgeInsets.fromLTRB(width*0.018056, height*0.0104167, width*0.018056, 0),
                                height: height*0.202083,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: width*0.000462963,
                                    color: Color.fromRGBO(255, 255, 255, 0.25),
                                  ),
                                  color: Colors.white.withOpacity(0.35),
                                  // borderRadius: BorderRadius.circular(width*0.01852),
                                ),
                                child: Column(
                                  crossAxisAlignment:CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      padding: EdgeInsets.only(left:width*0.0111),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          RichText(text: TextSpan(
                                              children: [
                                                whiteTitleSpan1("종합상담"),
                                                whiteTitleSpan2("창구"),
                                              ]
                                          ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        BlueButton(
                                            text : ["개인대출","/","전세대출"],
                                            buttonNum:[1,2,1]
                                        ),
                                        BlueButton(
                                            text : ["펀드(신탁)\n신규","/","해지"],
                                            buttonNum : [1,2,1]
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        BlueButton(
                                            text : ["개인형 IRP\n신규","/","해지"],
                                            buttonNum : [1,2,1]
                                        ),
                                        BlueButton(
                                            text : ["외환\n","(환전/송금)"],
                                            buttonNum: [1,3]
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        BlueButton(
                                            text : ["시금고업무\n","(채권매입, 과오납 환급)"],
                                            buttonNum:[1,3]
                                        ),
                                        BlueButton(
                                            text : ["전기통신금융사기"],
                                            buttonNum: [1]
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          MainButtonBottom(),
        ],
      ),
    );
  }
}

class BlueButton extends StatefulWidget {
  final List<String> text;
  final List<int> buttonNum;
  const BlueButton({Key key, this.text, this.buttonNum}) : super(key: key);

  @override
  _BlueButtonState createState() => _BlueButtonState();
}

class _BlueButtonState extends State<BlueButton> {
  bool isActive = false;

  TextSpan blueButtonSpan1(String text, bool isActive){
    return TextSpan(
        text:text,
        style: TextStyle(
          fontFamily: 'nanumSquare',
          fontSize: MediaQuery.of(context).size.width*0.02037,
          color: isActive ? Color.fromRGBO(255, 255, 255, 0.7) : Colors.white,
          letterSpacing: -(MediaQuery.of(context).size.width*0.0009167),
          height: 1.27,
          fontWeight: FontWeight.w700,
          shadows: <Shadow>[
            Shadow(
              offset: Offset(0.0, 3.0),
              blurRadius: 3.0,
              color: Color.fromRGBO(0, 45, 144, 0.16),
            ),
          ],
        )
    );
  }

  TextSpan blueButtonSpan2(String text, bool isActive){
    return TextSpan(
        text:text,
        style: TextStyle(
          fontFamily: 'nanumSquare',
          fontSize: MediaQuery.of(context).size.width*0.02037,
          color: isActive ? Color.fromRGBO(255, 255, 255, 0.7) : Colors.white,
          letterSpacing: -(MediaQuery.of(context).size.width*0.0009167),
          height: 1.27,
          fontWeight: FontWeight.w400,
          shadows: <Shadow>[
            Shadow(
              offset: Offset(0.0, 3.0),
              blurRadius: 3.0,
              color: Color.fromRGBO(0, 45, 144, 0.16),
            ),
          ],
        )
    );
  }

  TextSpan blueButtonSpan3(String text, bool isActive){
    return TextSpan(
        text:text,
        style: TextStyle(
          fontFamily: 'nanumSquare',
          fontSize: MediaQuery.of(context).size.width*0.01481,
          color: isActive ? Color.fromRGBO(255, 255, 255, 0.7) : Colors.white,
          letterSpacing: -(MediaQuery.of(context).size.width*0.00067),
          height: 1.75,
          fontWeight: FontWeight.w400,
          shadows: <Shadow>[
            Shadow(
              offset: Offset(0.0, 3.0),
              blurRadius: 3.0,
              color: Color.fromRGBO(0, 45, 144, 0.16),
            ),
          ],
        )
    );
  }

  TextSpan blueButtonSpan4(String text, bool isActive){
    return TextSpan(
        text:text,
        style: TextStyle(
          fontFamily: 'nanumSquare',
          fontSize: MediaQuery.of(context).size.width*0.02037,
          color: isActive ? Color.fromRGBO(255, 255, 255, 0.7) : Colors.white,
          letterSpacing: -(MediaQuery.of(context).size.width*0.0015278),
          height: 1.27,
          fontWeight: FontWeight.w700,
          shadows: <Shadow>[
            Shadow(
              offset: Offset(0.0, 3.0),
              blurRadius: 3.0,
              color: Color.fromRGBO(0, 45, 144, 0.16),
            ),
          ],
        )
    );
  }

  TextSpan blueButtonSpan5(String text, bool isActive){
    return TextSpan(
        text:text,
        style: TextStyle(
          fontFamily: 'nanumSquare',
          fontSize: MediaQuery.of(context).size.width*0.02037,
          color: isActive ? Color.fromRGBO(255, 255, 255, 0.7) : Colors.white,
          letterSpacing: -(MediaQuery.of(context).size.width*0.0015278),
          height: 1.27,
          fontWeight: FontWeight.w400,
          shadows: <Shadow>[
            Shadow(
              offset: Offset(0.0, 3.0),
              blurRadius: 3.0,
              color: Color.fromRGBO(0, 45, 144, 0.16),
            ),
          ],
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return GestureDetector(
      child: Padding(
        padding: EdgeInsets.fromLTRB(0,height*0.0104167,0,0),
        child: Container(
          alignment: Alignment.center,
          width: width*0.1740,
          height: height*0.04583,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(width*0.01852),
            boxShadow: [
              BoxShadow(
                color: Color.fromRGBO(55, 104, 233, 0.41),
                spreadRadius: 5,
                blurRadius: 40,
                offset: Offset(10, 10),
              )
            ],
            image: DecorationImage(
              image: isActive ? AssetImage("assets/buttons/btn_blue_pressed.png") : AssetImage("assets/buttons/btn_blue_default.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
                children: widget.buttonNum.asMap().map((index,value) {
                  switch(value){
                    case 1:
                      return MapEntry(index, blueButtonSpan1(widget.text[index], isActive));
                    case 2:
                      return MapEntry(index, blueButtonSpan2(widget.text[index], isActive));
                    case 3:
                      return MapEntry(index, blueButtonSpan3(widget.text[index], isActive));
                    case 4:
                      return MapEntry(index, blueButtonSpan4(widget.text[index], isActive));
                    case 5:
                      return MapEntry(index, blueButtonSpan5(widget.text[index], isActive));
                    default:
                      return null;
                  }
                }).values.toList()
            ),
          ),
        ),
      ),
      onTapDown: (TapDownDetails details) {
        setState(() {
          isActive = true;
        });
      },
      onTapUp: (TapUpDetails details) {
        setState(() {
          isActive = false;
        });
        // if(widget.onClick != null) widget.onClick();
      },
      onTapCancel: () {
        setState(() {
          isActive = false;
        });
      },
    );
  }
}
