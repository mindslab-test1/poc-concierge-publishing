import 'package:flutter/material.dart';

class MainButtonBottom extends StatefulWidget {
  @override
  _MainButtonBottomState createState() => _MainButtonBottomState();
}

class _MainButtonBottomState extends State<MainButtonBottom> {
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    var width = mediaQuery.size.width;
    var height = mediaQuery.size.height;

    return Padding(
      padding: EdgeInsets.only(top:height*0.01667),
      child: Container(
        width: width*0.4037,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                BottomButtons(
                  text: '직원 호출',
                  isGold: true,
                ),
                SizedBox(width: width*0.01852),
                BottomButtons(
                  text: '예약',
                  isGold: true,
                ),
              ],
            ),
            BottomButtons(
              text: '대화종료',
              isGold: false,
            ),
          ],
        ),
      ),
    );
  }
}

class BottomButtons extends StatefulWidget {
  final text;
  final bool isGold;
  const BottomButtons({Key key, this.text, this.isGold}) : super(key: key);

  @override
  _BottomButtonsState createState() => _BottomButtonsState();
}

class _BottomButtonsState extends State<BottomButtons> {
  bool isActive = false;

  Text goldButtonSpan(String text, bool isActive){
    return Text(
        text,
        style: TextStyle(
          fontFamily: 'nanumSquare',
          fontSize: MediaQuery.of(context).size.width*0.020327,
          color: isActive ? Color.fromRGBO(255, 255, 255, 0.7) : Color.fromRGBO(255, 255, 255, 0.92),
          letterSpacing: -(MediaQuery.of(context).size.width*0.0009167),
          height: 1.09,
          fontWeight: FontWeight.w700,
          shadows: <Shadow>[
            Shadow(
              offset: Offset(0.0, 3.0),
              blurRadius: 3.0,
              color: Color.fromRGBO(144, 122, 0, 0.16),
            ),
          ],
        )
    );
  }

  Text silverButtonSpan(String text, bool isActive){
    return Text(
        text,
        style: TextStyle(
          fontFamily: 'nanumSquare',
          fontSize: MediaQuery.of(context).size.width*0.020327,
          color: isActive ? Color.fromRGBO(255, 255, 255, 0.7) : Color.fromRGBO(255, 255, 255, 0.92),
          letterSpacing: -(MediaQuery.of(context).size.width*0.0009167),
          height: 1.09,
          fontWeight: FontWeight.w700,
          shadows: <Shadow>[
            Shadow(
              offset: Offset(0.0, 3.0),
              blurRadius: 3.0,
              color: Color.fromRGBO(144, 144, 144, 0.16),
            ),
          ],
        )
    );
  }

  AssetImage goldPressed = AssetImage("assets/buttons/btn_gold_pressed.png");
  AssetImage goldUnpressed = AssetImage("assets/buttons/btn_gold_default.png");
  AssetImage silverPressed = AssetImage("assets/buttons/btn_silver_pressed.png");
  AssetImage silverUnpressed = AssetImage("assets/buttons/btn_silver_default.png");
  Color goldBoxShadow = Color.fromRGBO(149, 97, 0, 0.41);
  Color silverBoxShadow = Color.fromRGBO(80, 80, 80, 0.41);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width*0.1111,
        height: MediaQuery.of(context).size.height*0.0375,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width*0.01852),
          boxShadow: [
            BoxShadow(
              color: widget.isGold? goldBoxShadow : silverBoxShadow,
              blurRadius: 30,
              offset: Offset(10, 10),
            )
          ],
          image: DecorationImage(
            image: isActive ? (widget.isGold? goldPressed:silverPressed) : (widget.isGold? goldUnpressed:silverUnpressed),
            fit: BoxFit.cover,
          ),
        ),
        child: widget.isGold ? goldButtonSpan(widget.text, isActive): silverButtonSpan(widget.text, isActive),
      ),
      onTapDown: (TapDownDetails details) {
        setState(() {
          this.isActive = true;
        });
      },
      onTapUp: (TapUpDetails details) {
        setState(() {
          this.isActive = false;
        });
        // if(widget.onClick != null) widget.onClick();
      },
      onTapCancel: () {
        setState(() {
          this.isActive = false;
        });
      },
    );
  }
}

