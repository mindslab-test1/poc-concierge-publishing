import 'dart:ui';
import 'package:flutter/material.dart';

class AudioBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return Container(
      padding: EdgeInsets.only(right: width*0.037),
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          SizedBox(height: height*0.1518229167),
          Container(
            width: width*0.4037,
            padding: EdgeInsets.fromLTRB(0, 0, width*0.01852, height*0.0104167),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                RichText(
                  text: TextSpan(
                      text: 'AI Banker',
                      style: TextStyle(
                          fontFamily: 'nanumSquare',
                          fontSize: width*0.0148,
                          letterSpacing: -(width*0.00067),
                          color: Color(0xff9a9a9a),
                          height:1,
                          fontWeight: FontWeight.w700
                      ),
                      children: [
                        TextSpan(text: '가 듣고 있어요.',
                            style: TextStyle(
                                fontWeight: FontWeight.w400
                            )
                        ),
                      ]),
                ),
                Padding(
                  padding: EdgeInsets.only(left: width*0.0074),
                  child: Image.asset(
                    'assets/ico_mic.png',
                    width: width*0.01852,
                    fit: BoxFit.fill,
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: width*0.4037,
            child: Stack(
              children: [
                Column(
                  children: [
                    ClipRect(
                      child: BackdropFilter(
                        filter: ImageFilter.blur(
                          sigmaX: width*0.00462963,
                          sigmaY: width*0.00462963,
                        ),
                        child: Container(
                          padding: EdgeInsets.fromLTRB(width*0.01852, height*0.01667, width*0.01852, height*0.01667),
                          width: width*0.4037,
                          height: height*0.105729167,
                          decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.4),
                            // borderRadius: BorderRadius.circular(width*0.01852),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Container(
                                      width: width*0.36111,
                                      child: Text(
                                        '개인 신용대출을 상담하러 왔는데 어떻게 해야하나요?',
                                        softWrap: true,
                                        overflow: TextOverflow.visible,
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700,
                                            fontSize: width*0.01852,
                                            color: Colors.white,
                                            height: 1.3),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Image.asset(
                                'assets/spectrum.png',
                                width: width*0.36574,
                                fit: BoxFit.fill,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
