import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shinhan/components/main_button_bottom.dart';

class SideMenuPopup extends StatefulWidget {
  @override
  _SideMenuPopupState createState() => _SideMenuPopupState();
}

class _SideMenuPopupState extends State<SideMenuPopup> {
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return Container(
      padding: EdgeInsets.only(right: width*0.037),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SizedBox(
                  height: height*0.29427083,
                ),
                Container(
                  width: width*0.4037,
                  child: Stack(
                    children: [
                      Column(
                        children: [
                          ClipRect(
                            child: BackdropFilter(
                              filter: ImageFilter.blur(
                                sigmaX: width*0.00565,
                                sigmaY: width*0.00565,
                              ),
                              child:
                              numberTicket(), // 번호표 뽑기
                              // wc(),  // 화장실
                              // location(),  // 지도
                              // weather(),  // 날씨
                              // information(),  // 문의하세요(죄송합니다 포함)
                              // calling(),
                            ),
                          ),
                          MainButtonBottom(),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget numberTicket() => Container(
    padding: EdgeInsets.all(MediaQuery.of(context).size.width*0.018056),
    width: MediaQuery.of(context).size.width*0.4037,
    height: MediaQuery.of(context).size.height*0.284375,
    decoration: BoxDecoration(
      border: Border.all(
        width: MediaQuery.of(context).size.width*0.000925926,
        color: Color.fromRGBO(255, 255, 255, 0.45),
      ),
      color: Colors.white.withOpacity(0.35),
      // borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width*0.01852),
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.0111),
              child: Text(
                '번호표 발급',
                style: TextStyle(
                  fontSize: MediaQuery.of(context).size.width*0.02222,
                  height:1,
                  fontWeight: FontWeight.w700,
                  color: Color(0xff3d3d3d),
                ),
              ),
            ),
          ],
        ),
        Container(
          width: MediaQuery.of(context).size.width*0.36667,
          height: MediaQuery.of(context).size.height*0.2393229167,
          padding: EdgeInsets.fromLTRB(0, MediaQuery.of(context).size.height*0.02083, 0, MediaQuery.of(context).size.height*0.027083),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width*0.01852),
              boxShadow: [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.1),
                  offset: Offset(6.0, 6.0),
                  blurRadius: 8.0,
                )
              ]
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width*0.01481, MediaQuery.of(context).size.height*0.003125, MediaQuery.of(context).size.width*0.01481, MediaQuery.of(context).size.height*0.0015625),
                    decoration: BoxDecoration(
                      color: Color(0xffececec),
                      borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width*0.01852),
                    ),
                    child: Text('서소문지점',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: MediaQuery.of(context).size.width*0.01852,
                          color: Color(0xff7f7f7f),
                          height:1,
                          fontWeight: FontWeight.w700
                      ),
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height*0.01667),
                  Image(
                    image: AssetImage('assets/img_ticket.png'),
                    width: MediaQuery.of(context).size.width*0.1852,
                    fit: BoxFit.fill,
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  // SizedBox(height: MediaQuery.of(context).size.height*0.023),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top:MediaQuery.of(context).size.height*0.001302083),
                        child: Text(
                          '발급기의 번호표를 받아주세요',
                          style: TextStyle(
                            fontSize: MediaQuery.of(context).size.width*0.0222,
                            fontWeight: FontWeight.w700,
                            letterSpacing: - (MediaQuery.of(context).size.width*0.001),
                            color: Color(0xff3d3d3d),
                            height:1,
                          ),
                        ),
                      ),
                      SizedBox(width: MediaQuery.of(context).size.width*0.0111),
                      Image(
                        image: AssetImage('assets/ico_arrow_right.png'),
                        width: MediaQuery.of(context).size.width*0.0222,
                        fit: BoxFit.fill,
                      )
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.01325),
                    child: Text(
                      '번호표를 가지고 잠시만 기다려 주세요.',
                      style: TextStyle(
                          fontSize: MediaQuery.of(context).size.width*0.01852,
                          fontWeight: FontWeight.w400,
                          letterSpacing: -(MediaQuery.of(context).size.width*0.01852*0.00083),
                          height: 1,
                          color: Color(0xff606972)),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ],
    ),
  );

  Widget wc() => Container(
    padding: EdgeInsets.all(MediaQuery.of(context).size.width*0.018056),
    width: MediaQuery.of(context).size.width*0.4037,
    height: MediaQuery.of(context).size.height*0.284375,
    decoration: BoxDecoration(
      border: Border.all(
        width: MediaQuery.of(context).size.width*0.000925926,
        color: Color.fromRGBO(255, 255, 255, 0.45),
      ),
      color: Colors.white.withOpacity(0.35),
      // borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width*0.01852),
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment:
      MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment:
          MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.0111),
              child: Text(
                '위치안내',
                style: TextStyle(
                  fontSize: MediaQuery.of(context).size.width*0.02222,
                  fontWeight: FontWeight.w700,
                  color: Color(0xff3d3d3d),
                  height:1,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width*0.0111),
              child: Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width*0.000556),
                    child: Image.asset(
                      'assets/ico_pin.png',
                      height: MediaQuery.of(context).size.height*0.0114583,
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                  Text(
                    '서소문지점',
                    style: TextStyle(
                      fontSize: MediaQuery.of(context).size.width*0.01852,
                      fontWeight: FontWeight.w700,
                      letterSpacing: -(MediaQuery.of(context).size.width*0.01852*0.00083),
                      height: 1.2,
                      color: Color(0xff606972),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
        Container(
          width: MediaQuery.of(context).size.width*0.3667,
          height: MediaQuery.of(context).size.height*0.2393229167,
          decoration: BoxDecoration(
            border: Border.all(
              color: Color.fromRGBO(255, 255, 255, 0.25),
            ),
            color: Colors.white,
            borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width*0.01852),
          ),
          child: Image(
            image: AssetImage('assets/img_wc.png'),
            width: MediaQuery.of(context).size.width*0.3667,
            fit: BoxFit.contain,
          ),
        ),
      ],
    ),
  );

  Widget location() => Container(
    padding: EdgeInsets.all(MediaQuery.of(context).size.width*0.018056),
    width: MediaQuery.of(context).size.width*0.4037,
    height: MediaQuery.of(context).size.height*0.284375,
    decoration: BoxDecoration(
      border: Border.all(
        width: MediaQuery.of(context).size.width*0.000925926,
        color: Color.fromRGBO(255, 255, 255, 0.45),
      ),
      color: Colors.white.withOpacity(0.35),
      // borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width*0.01852),
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment:
      MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment:
          MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.0111),
              child: Text(
                '위치안내',
                style: TextStyle(
                  fontSize: MediaQuery.of(context).size.width*0.02222,
                  fontWeight: FontWeight.w700,
                  color: Color(0xff3d3d3d),
                  height:1,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width*0.0111),
              child: Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width*0.000556),
                    child: Image.asset(
                      'assets/ico_pin.png',
                      height: MediaQuery.of(context).size.height*0.0114583,
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                  Text(
                    '서소문지점',
                    style: TextStyle(
                      fontSize: MediaQuery.of(context).size.width*0.01852,
                      fontWeight: FontWeight.w700,
                      letterSpacing: -(MediaQuery.of(context).size.width*0.01852*0.00083),
                      height: 1.2,
                      color: Color(0xff606972),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
        Image(
          image: AssetImage('assets/img_map.png'),
          width: MediaQuery.of(context).size.width*0.3667,
          height: MediaQuery.of(context).size.height*0.2393229167,
          fit: BoxFit.fill,
        ),
      ],
    ),
  );

  Widget weather() => Container(
    padding: EdgeInsets.all(MediaQuery.of(context).size.width*0.018056),
    width: MediaQuery.of(context).size.width*0.4037,
    height: MediaQuery.of(context).size.height*0.284375,
    decoration: BoxDecoration(
      border: Border.all(
        width: MediaQuery.of(context).size.width*0.000925926,
        color: Color.fromRGBO(255, 255, 255, 0.45),
      ),
      color: Colors.white.withOpacity(0.35),
      // borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width*0.01852),
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.0111),
              child: Text(
                '날씨안내',
                style: TextStyle(
                  fontSize: MediaQuery.of(context).size.width*0.02222,
                  fontWeight: FontWeight.w700,
                  color: Color(0xff3d3d3d),
                  height:1,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: MediaQuery.of(context).size.width*0.0111),
              child: Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width*0.000556),
                    child: Image.asset(
                      'assets/ico_pin.png',
                      height: MediaQuery.of(context).size.height*0.0114583,
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                  Text(
                    '서울시, 중구',
                    style: TextStyle(
                      fontSize: MediaQuery.of(context).size.width*0.01852,
                      fontWeight: FontWeight.w700,
                      letterSpacing: -(MediaQuery.of(context).size.width*0.01852*0.00083),
                      height: 1.2,
                      color: Color(0xff606972),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
        Image(
          image: AssetImage('assets/img_weather.png'),
          width: MediaQuery.of(context).size.width*0.3667,
          height: MediaQuery.of(context).size.height*0.2393229167,
          fit: BoxFit.fill,
        ),
      ],
    ),
  );

  Widget information() => Container(
    padding: EdgeInsets.all(MediaQuery.of(context).size.width*0.018056),
    width: MediaQuery.of(context).size.width*0.4037,
    height: MediaQuery.of(context).size.height*0.284375,
    decoration: BoxDecoration(
      border: Border.all(
        width: MediaQuery.of(context).size.width*0.000925926,
        color: Color.fromRGBO(255, 255, 255, 0.45),
      ),
      color: Colors.white.withOpacity(0.35),
      // borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width*0.01852),
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.0111),
              child: Text(
                '안내',
                style: TextStyle(
                  fontSize: MediaQuery.of(context).size.width*0.02222,
                  fontWeight: FontWeight.w700,
                  color: Color(0xff3d3d3d),
                  height:1,
                ),
              ),
            ),
          ],
        ),
        Image(
          image: AssetImage('assets/img_app.png'),
          // image: AssetImage('assets/img_sorry.png'),
          width: MediaQuery.of(context).size.width*0.3667,
          height: MediaQuery.of(context).size.height*0.2393229167,
          fit: BoxFit.fill,
        ),
      ],
    ),
  );

  Widget calling() => Container(
    padding: EdgeInsets.all(MediaQuery.of(context).size.width*0.018056),
    width: MediaQuery.of(context).size.width*0.4037,
    height: MediaQuery.of(context).size.height*0.284375,
    decoration: BoxDecoration(
      border: Border.all(
        width: MediaQuery.of(context).size.width*0.000925926,
        color: Color.fromRGBO(255, 255, 255, 0.45),
      ),
      color: Colors.white.withOpacity(0.35),
      // borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width*0.01852),
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.0111),
              child: Text(
                '직원 호출 안내',
                style: TextStyle(
                  fontSize: MediaQuery.of(context).size.width*0.02222,
                  fontWeight: FontWeight.w700,
                  color: Color(0xff3d3d3d),
                  height:1,
                ),
              ),
            ),
          ],
        ),
        Image(
          image: AssetImage('assets/img_waiting.png'),
          width: MediaQuery.of(context).size.width*0.3667,
          height: MediaQuery.of(context).size.height*0.2393229167,
          fit: BoxFit.fill,
        ),
      ],
    ),
  );
}
