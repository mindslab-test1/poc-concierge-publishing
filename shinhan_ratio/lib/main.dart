import 'package:flutter/material.dart';
import 'components/audioAnimation/audioBox.dart';
import 'components/time/current_time.dart';
import 'components/side_menu_popup.dart';
import 'components/main_button_top.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    var width = mediaQuery.size.width;
    var height = mediaQuery.size.height;

    // var physicalPixelWidth = mediaQuery.size.width * mediaQuery.devicePixelRatio;
    // var physicalPixelHeight = mediaQuery.size.height * mediaQuery.devicePixelRatio;
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      appBar: null,
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/bg_shinhan.jpg"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(width*0.037, height*0.02083, width*0.037, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image(
                  image: AssetImage('assets/img_shinhan_logo.png'),
                  width: width * 0.1851,
                  height: height * 0.0239583,
                  fit: BoxFit.contain,
                ),
                CurrentTime(),
              ],
            ),
          ),
          Container(
            child: Image(
              image: AssetImage('assets/img_YSM_Wait02.png'),
              width: width,
              height: height,
              fit: BoxFit.contain,
            ),
          ),
          AudioBox(),
          // MainButtonTop(),
          SideMenuPopup(),
        ],
      ),
    );
  }
}