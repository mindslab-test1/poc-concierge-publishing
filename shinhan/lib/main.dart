import 'package:flutter/material.dart';
import 'components/audioAnimation/audioBox.dart';
import 'components/time/current_time.dart';
import 'components/side_menu_popup.dart';
import 'components/main_button_top.dart';

void main() => runApp(
    MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/bg_shinhan.jpg"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(80, 80, 80, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image(
                  image: AssetImage('assets/img_shinhan_logo.png'),
                  width: 400,
                  height: 92,
                ),
                CurrentTime(),
              ],
            ),
          ),
          Container(
            child: Image(image: AssetImage('assets/img_YSM_Wait02.png')),
          ),
          AudioBox(),
          // MainButtonTop(),
          SideMenuPopup(),
        ],
      ),
    );
  }
}
