import 'dart:ui';

import 'package:flutter/material.dart';

class AudioBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 80),
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          SizedBox(height: 580),
          Container(
            width: 872,
            padding: EdgeInsets.fromLTRB(0, 0, 40, 40),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                RichText(
                  text: TextSpan(
                      text: 'AI Banker',
                      style: TextStyle(
                        fontFamily: 'nanumSquare',
                        fontSize: 32,
                        letterSpacing: -1.44,
                        color: Color(0xff9a9a9a),
                        height:1,
                        fontWeight: FontWeight.w700
                      ),
                      children: [
                        TextSpan(text: '가 듣고 있어요.',
                          style: TextStyle(
                            fontWeight: FontWeight.w400
                          )
                        ),
                      ]),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 16),
                  child: Image.asset(
                    'assets/ico_mic.png',
                    width: 40,
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: 872,
            child: Stack(
              children: [
                Column(
                  children: [
                    ClipRect(
                      child: BackdropFilter(
                        filter: ImageFilter.blur(
                          sigmaX: 10,
                          sigmaY: 10,
                        ),
                        child: Container(
                          padding: EdgeInsets.fromLTRB(40, 64, 40, 64),
                          width: 872,
                          height: 406,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Color.fromRGBO(255, 255, 255, 0.25),
                            ),
                            color: Color.fromRGBO(0, 0, 0, 0.4),
                            borderRadius: BorderRadius.circular(40),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Container(
                                      width: 790,
                                      child: Text(
                                        '개인 신용대출을 상담하러 왔는데 어떻게 해야하나요?',
                                        softWrap: true,
                                        overflow: TextOverflow.visible,
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w700,
                                            fontSize: 40,
                                            color: Colors.white,
                                            height: 1.3),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Image.asset(
                                'assets/spectrum.png',
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
