import 'package:flutter/material.dart';

class CommonButton extends StatefulWidget {
  final String activeImage;
  final String inactiveImage;
  final double width;
  final double height;
  // final Function onClick;
  final bool tempisActive;

  const CommonButton(
      {
        required Key key,
        required this.activeImage,
        required this.inactiveImage,
        required this.width,
        required this.height,
        this.tempisActive = true,
        // required this.onClick,
      })
      : super(key: key);

  @override
  _CommonButtonState createState() => _CommonButtonState();
}

class _CommonButtonState extends State<CommonButton> {
  bool isActive = false;

  void clickHandler() {
    setState(() {
      this.isActive = !this.isActive;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return GestureDetector(
      child: Padding(
        padding: EdgeInsets.fromLTRB(size.width*0.008, size.width*0.007, size.width*0.008, size.width*0.0085),
        child: Container(
          width: widget.width,
          height: widget.height,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(size.width*0.02),
              boxShadow: [
                BoxShadow(
                    color: (isActive && widget.tempisActive)
                        ?Colors.transparent:Color.fromRGBO(0, 0, 0, 0.2),
                    spreadRadius: 0,
                    blurRadius: 30,
                    offset: Offset(size.width*0.005, size.width*0.006)
                )
              ]
          ),
          child: Image(
            fit: BoxFit.fill,
            image: isActive && widget.tempisActive
                ? AssetImage(widget.activeImage)
                : AssetImage(widget.inactiveImage),
          ),
        ),
      ),
      onTapDown: (TapDownDetails details) {
        setState(() {
          this.isActive = true;
        });
      },
      onTapUp: (TapUpDetails details) {
        setState(() {
          this.isActive = false;
        });
        // if(widget.onClick != null) widget.onClick();
      },
      onTapCancel: () {
        setState(() {
          this.isActive = false;
        });
      },
    );
  }
}
