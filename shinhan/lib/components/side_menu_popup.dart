import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shinhan/components/main_button_bottom.dart';

class SideMenuPopup extends StatefulWidget {
  @override
  _SideMenuPopupState createState() => _SideMenuPopupState();
}

class _SideMenuPopupState extends State<SideMenuPopup> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 80),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SizedBox(
                  height: 1130,
                ),
                Container(
                  width: 872,
                  child: Stack(
                    children: [
                      Column(
                        children: [
                          ClipRect(
                            child: BackdropFilter(
                              filter: ImageFilter.blur(
                                sigmaX: 12,
                                sigmaY: 12,
                              ),
                              child:
                                numberTicket(), // 번호표 뽑기
                                // wc(),  // 화장실
                                // location(),  // 지도
                                // weather(),  // 날씨
                                // information(),  // 문의하세요(죄송합니다 포함)
                            ),
                          ),
                          MainButtonBottom(),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget numberTicket() => Container(
    padding: EdgeInsets.all(38),
    width: 872,
    height: 1092,
    decoration: BoxDecoration(
      border: Border.all(
        width: 2,
        color: Color.fromRGBO(255, 255, 255, 0.45),
      ),
      color: Colors.white.withOpacity(0.4),
      borderRadius: BorderRadius.circular(40),
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding:
              const EdgeInsets.only(left: 24),
              child: Text(
                '번호표 발급',
                style: TextStyle(
                    fontSize: 48,
                    height:1,
                    fontWeight: FontWeight.w700,
                    color: Color(0xff3d3d3d),
                ),
              ),
            ),
          ],
        ),
        Container(
          width: 792,
          height:919,
          padding: EdgeInsets.fromLTRB(0, 80, 0, 104),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(40),
            boxShadow: [
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.1),
                offset: Offset(6.0, 6.0),
                blurRadius: 8.0,
              )
            ]
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(32, 8, 32, 6),
                decoration: BoxDecoration(
                  color: Color(0xffececec),
                  borderRadius: BorderRadius.circular(40),
                ),
                child: Text('서소문지점',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 40,
                    color: Color(0xff7f7f7f),
                    height:1,
                    fontWeight: FontWeight.w700
                  ),
                ),
              ),
              SizedBox(height: 64),
              Image(
                image: AssetImage('assets/img_ticket.png'),
                width: 400,
              ),
              SizedBox(height:96),
              Row(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top:5),
                    child: Text(
                      '발급기의 번호표를 받아주세요',
                      style: TextStyle(
                        fontSize: 48,
                        fontWeight: FontWeight.w700,
                        letterSpacing: -2.16,
                        color: Color(0xff3d3d3d),
                        height:1,
                      ),
                    ),
                  ),
                  SizedBox(width: 24),
                  Image(image: AssetImage('assets/ico_arrow_right.png'))
                ],
              ),
              Container(
                padding: EdgeInsets.only(top: 64),
                child: Text(
                  '번호표를 가지고 잠시만 기다려 주세요.',
                  style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.w400,
                      letterSpacing: -1.8,
                      height: 1.5,
                      color: Color(0xff606972)),
                ),
              )
            ],
          ),
        ),
      ],
    ),
  );

  Widget wc() => Container(
    padding: EdgeInsets.all(40),
    width: 872,
    height: 1092,
    decoration: BoxDecoration(
      border: Border.all(
        width: 2,
        color: Color.fromRGBO(255, 255, 255, 0.45),
      ),
      color: Colors.white.withOpacity(0.4),
      borderRadius: BorderRadius.circular(40),
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment:
      MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment:
          MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding:
              const EdgeInsets.only(left: 24),
              child: Text(
                '위치안내',
                style: TextStyle(
                  fontSize: 48,
                  fontWeight: FontWeight.w700,
                  color: Color(0xff3d3d3d),
                  height:1,
                ),
              ),
            ),
            Padding(
              padding:
              const EdgeInsets.only(right: 24),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right:12),
                    child: Image.asset(
                      'assets/ico_pin.png',
                      height: 44,
                    ),
                  ),
                  Text(
                    '서소문지점',
                    style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.w700,
                      letterSpacing: -1.8,
                      height: 1.2,
                      color: Color(0xff606972),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
        Container(
          width: 792,
          height:919,
          decoration: BoxDecoration(
              border: Border.all(
                color: Color.fromRGBO(255, 255, 255, 0.25),
              ),
              color: Colors.white,
              borderRadius: BorderRadius.circular(40),
          ),
          child: Image(
            image: AssetImage('assets/img_wc.png'),
            width: 792,
          ),
        ),
      ],
    ),
  );

  Widget location() => Container(
    padding: EdgeInsets.all(40),
    width: 872,
    height: 1092,
    decoration: BoxDecoration(
      border: Border.all(
        width: 2,
        color: Color.fromRGBO(255, 255, 255, 0.45),
      ),
      color: Colors.white.withOpacity(0.4),
      borderRadius: BorderRadius.circular(40),
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment:
      MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment:
          MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding:
              const EdgeInsets.only(left: 24),
              child: Text(
                '위치안내',
                style: TextStyle(
                  fontSize: 48,
                  fontWeight: FontWeight.w700,
                  color: Color(0xff3d3d3d),
                  height:1,
                ),
              ),
            ),
            Padding(
              padding:
              const EdgeInsets.only(right: 24),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right:12),
                    child: Image.asset(
                      'assets/ico_pin.png',
                      height: 44,
                    ),
                  ),
                  Text(
                    '서소문지점',
                    style: TextStyle(
                        fontSize: 40,
                        fontWeight: FontWeight.w700,
                        letterSpacing: -1.8,
                        height: 1.2,
                        color: Color(0xff606972),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
        Image(
          image: AssetImage('assets/img_map.png'),
          width: 792,
        ),
      ],
    ),
  );

  Widget weather() => Container(
    padding: EdgeInsets.all(40),
    width: 872,
    height: 1092,
    decoration: BoxDecoration(
        border: Border.all(
          width: 2,
          color: Color.fromRGBO(255, 255, 255, 0.45),
        ),
        color: Colors.white.withOpacity(0.4),
        borderRadius: BorderRadius.circular(40),
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding:
              const EdgeInsets.only(left: 24),
              child: Text(
                '날씨안내',
                style: TextStyle(
                  fontSize: 48,
                  fontWeight: FontWeight.w700,
                  color: Color(0xff3d3d3d),
                  height:1,
                ),
              ),
            ),
            Padding(
              padding:
              const EdgeInsets.only(right: 24),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right:12),
                    child: Image.asset(
                      'assets/ico_pin.png',
                      height: 44,
                    ),
                  ),
                  Text(
                    '서울시, 중구',
                    style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.w700,
                      letterSpacing: -1.8,
                      height: 1.2,
                      color: Color(0xff606972),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
        Image(
          image: AssetImage('assets/img_weather.png'),
          width: 792,
        ),
      ],
    ),
  );

  Widget information() => Container(
    padding: EdgeInsets.all(40),
    width: 872,
    height: 1092,
    decoration: BoxDecoration(
      border: Border.all(
        width: 2,
        color: Color.fromRGBO(255, 255, 255, 0.45),
      ),
      color: Colors.white.withOpacity(0.4),
      borderRadius: BorderRadius.circular(40),
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment:
          MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding:
              const EdgeInsets.only(left: 24),
              child: Text(
                '안내',
                style: TextStyle(
                  fontSize: 48,
                  fontWeight: FontWeight.w700,
                  color: Color(0xff3d3d3d),
                  height:1,
                ),
              ),
            ),
          ],
        ),
        Image(
          image: AssetImage('assets/img_app.png'),
          // image: AssetImage('assets/img_sorry.png'),
          width: 792,
        ),
      ],
    ),
  );
}
