import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:timer_builder/timer_builder.dart';

class CurrentTime extends StatefulWidget {
  const CurrentTime({key}) : super(key: key);

  @override
  _CurrentTimeState createState() => _CurrentTimeState();
}

class _CurrentTimeState extends State<CurrentTime> {
  String weekNameEngToKor(String eng){
    if(eng == 'Monday') return '월';
    else if(eng == 'Tuesday') return '화';
    else if(eng == 'Wednesday') return '수';
    else if(eng == 'Thursday') return '목';
    else if(eng == 'Friday') return '금';
    else if(eng == 'Saturday') return '토';
    else return '일';
  }

  @override
  Widget build(BuildContext context) {
    return TimerBuilder.periodic(
      const Duration(seconds: 1),
      builder: (context) {
        return Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: [
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    formatDate(
                      DateTime.now(),
                      [yyyy, '.', mm, '.', dd,'. (${weekNameEngToKor(DateFormat('EEEE').format(DateTime.now()))})   ', am,]
                    ),
                    style: TextStyle(
                      color: Color(0xff676767),
                      fontSize: 48,
                      fontFamily: 'nanumSquare',
                      fontWeight: FontWeight.w700,
                      height: 1,
                    ),
                  ),
                  SizedBox(height:8),
                ],
              ),
              SizedBox(width: 20),
              Text(
                formatDate(
                    DateTime.now(),
                    [hh, ' : ', nn]
                ),
                style: TextStyle(
                  color: Color(0xff676767),
                  fontSize: 64,
                  fontFamily: 'nanumSquare',
                  fontWeight: FontWeight.w800,
                  height: 1,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
