import 'package:flutter/material.dart';

class ButtonStyleType {
  TextStyle textStyle1 = TextStyle(

    fontFamily: 'nanumSquare',
    fontSize: 44,
    fontWeight: FontWeight.w700,
    height:1.09,
    letterSpacing: -1.98,
    color: Color(0xff084da3),
  );

  TextStyle textStyle2 = TextStyle(
    fontSize: 44,
    fontWeight: FontWeight.w400,
    height:1.09,
    letterSpacing: -1.98,
    color: Color(0xff084da3),
  );

}